+++
title = "Graham Scheaffer"
date = 2019-11-22
+++
## About me
I am a university student at Dakota State University. I enjoy programming
computers and using linux. I also like playing games on my PC. You can see my
setup [here][1]. 

Most my code is available on my [Gitlab][2].

## Blog
You can read my blog posts [here](blog)

## Contact
You can contact me at [me@gisch.dev](mailto:me@gisch.dev)

[1]: https://pcpartpicker.com/user/pythondude325/saved/R3ZQZL
[2]: https://gitlab.com/pythondude325